// Custom Expense Claim Form
// Author: James Attard (Cloudreach Ltd.)
// Date: April 2013

var CLAIMS_SPREADSHEET_ID = "0AvqYxIrouBHfdGdJb29MZ0o0ZmhxcmE4Rlc4dTBtdFE";
var APPROVALS_SPREADSHEET_ID = "0AvqYxIrouBHfdFZzY1B3dzllRDNvZk9IOWxHYVlzOWc";
var APPROVAL_FORM_URL = "https://docs.google.com/a/cloudreach.co.uk/spreadsheet/viewform?formkey=dFZzY1B3dzllRDNvZk9IOWxHYVlzOWc6MA";
var STATE_MANAGER_EMAIL = "james.attard@cloudreach.com";
var STATE_APPROVED = "APPROVED";
var STATE_DENIED = "DENIED";
var COLUMN_STATE = 6;
var COLUMN_COMMENT = 7;


function doGet() {
  var app = UiApp.createApplication().setTitle('Company Expense Claim Form');;
  var form = app.createFormPanel();
   
  // GRID LAYOUT
  var grid = app.createGrid(8,2).setId('expensesGrid');
  var expEmpLabel = app.createLabel("Employee Email");
  var expEmpTextbox = app.createTextBox().setWidth('300px').setName('empemail');
  var expAmountLabel = app.createLabel("Amount (£)");
  var expAmountTextbox = app.createTextBox().setWidth('75px').setName('expamount'); 
  var expTypeLabel = app.createLabel('Expense Type');
  var expTypeList = app.createListBox().setName('Expense List').setWidth('120px').setName('exptype');
      expTypeList.addItem('Travel');    
      expTypeList.addItem('Accomodation');
      expTypeList.addItem('Software');  
      expTypeList.addItem('Other');
  var expDescLabel = app.createLabel("Description");
  var expDescTextarea = app.createTextArea().setWidth('300px').setHeight('100px').setName('expdesc'); 
  var expDateLabel = app.createLabel('Expense Date');
  var expDateTextbox = app.createDateBox().setWidth('150px').setName('expdate');
  var expManagerLabel = app.createLabel("Manager Email");
  var expManagerTextbox = app.createTextBox().setWidth('300px').setName('manageremail');
  var upLoadTypeLabel = app.createLabel('File Upload');
  var upLoad = (app.createFileUpload().setName('thefile'));
  var submitButton = app.createSubmitButton('<B>Submit</B>');
  var warning = app.createHTML('<B>PLEASE WAIT WHILE DATA IS UPLOADING<B>').setStyleAttribute('background','yellow').setVisible(false)
  
  grid.setWidget(0, 0, expEmpLabel)
      .setWidget(0, 1, expEmpTextbox)
      .setWidget(1, 0, expAmountLabel)
      .setWidget(1, 1, expAmountTextbox)
      .setWidget(2, 0, expTypeLabel)
      .setWidget(2, 1, expTypeList)
      .setWidget(3, 0, expDescLabel)
      .setWidget(3, 1, expDescTextarea)
      .setWidget(4, 0, expDateLabel)
      .setWidget(4, 1, expDateTextbox)
      .setWidget(5, 0, expManagerLabel)
      .setWidget(5, 1, expManagerTextbox)
      .setWidget(6, 0, upLoadTypeLabel)
      .setWidget(6, 1, upLoad)
      .setWidget(7, 0, submitButton)
      .setWidget(7, 1, warning)
  
  
  var cliHandler = app.createClientHandler().forTargets(warning).setVisible(true)
  submitButton.addClickHandler(cliHandler);  
  
  form.add(grid);
  app.add(form);
    
  //var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  //spreadsheet.show(app); 
  return app;
 }





function doPost(eventInfo) {
  var app = UiApp.getActiveApplication();
  var formattedDate = Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd HH:mm:ss");
  var flag = 1; // flag == 1: OK, flag == 0: ERROR
  
  
  // Upload file
  var fileBlob = eventInfo.parameter.thefile;
  var byteArrayLength = fileBlob.getBytes().length;
  
  
  // Generate error if no file was uploaded 
  if(byteArrayLength == 0){
    flag = 0;
    var label = app.createLabel('no file uploaded. please upload file.');
    app.add(label);
    return app;
  }
   
  
  // If expense receipt uploaded, continue workflow 
  if(flag == 1){ 
  var doc = DocsList.createFile(fileBlob);
  var fileId = doc.getId();  
  doc.addToFolder(DocsList.getFolder("Receipts"));
  var app = UiApp.getActiveApplication();
    
  // Display a confirmation message
  var label = app.createLabel('Claim Expense submitted successfully. An email has been sent to your line manager.');
  app.add(label);
   
  
  // Update spreadsheet
  //var sheet = SpreadsheetApp.getActiveSheet();
  var sheet = SpreadsheetApp.openById(CLAIMS_SPREADSHEET_ID);
  var lastRow = sheet.getLastRow()+1;
    
  var lastCellA = sheet.getRange("A"+lastRow);
  lastCellA.setValue(formattedDate);      
  var lastCellB = sheet.getRange("B"+lastRow);
  lastCellB.setValue(eventInfo.parameter.empemail);   
  var lastCellC = sheet.getRange("C"+lastRow);
  lastCellC.setValue(eventInfo.parameter.expamount); 
  var lastCellD = sheet.getRange("D"+lastRow);
  lastCellD.setValue(eventInfo.parameter.expdesc);
  var lastCellE = sheet.getRange("E"+lastRow);
  lastCellE.setValue(eventInfo.parameter.manageremail);      
  var lastCellG = sheet.getRange("G"+lastRow);
  lastCellG.setValue("https://docs.google.com/a/cloudreach.co.uk/file/d/"+fileId+"/edit");
  var lastCellH = sheet.getRange("H"+lastRow);
  lastCellH.setValue(eventInfo.parameter.exptype);
  var lastCellI = sheet.getRange("I"+lastRow);
  lastCellI.setValue(eventInfo.parameter.expdate);  
    
  onReportOrApprovalSubmit();  
    
  return app;  
  }
}


function onReportOrApprovalSubmit() {
   // This is the Custom Expense Report Spreadsheet
  //var ss = SpreadsheetApp.getActiveSpreadsheet();
  var ss = SpreadsheetApp.openById(CLAIMS_SPREADSHEET_ID);
  var expenseSheet = ss.getSheets()[0];

  // Also open the Approvals Spreadsheet
  var approvalsSpreadsheet = SpreadsheetApp.openById(APPROVALS_SPREADSHEET_ID);
  var approvalsSheet = approvalsSpreadsheet.getSheets()[0];

  // Fetch all the data from the Expense Report Spreadsheet
  var data = getRowsData(expenseSheet);

  // Fetch all the data from the Approvals Spreadsheet
  var approvalsData = getRowsData(approvalsSheet);
    

  // For every expense report
  for (var i = 0; i < data.length; ++i) {
    var row = data[i];
    row.rowNumber = i + 2;
    if (!row.state) {
      // This is a new Expense Report.
      // Email the manager to request his approval.
      sendReportToManager(row);
      // Update the state of the report to avoid email sending multiple emails
      // to managers about the same report.
      expenseSheet.getRange(row.rowNumber, COLUMN_STATE).setValue(row.state);
    } else if (row.state == STATE_MANAGER_EMAIL) {
      // This expense report has already been submitted to a manager for approval.
      // Check if the manager has accepted or rejected the report in the Approval Spreadsheet.
      for (var j = 0; j < approvalsData.length; ++j) {
        var approval = approvalsData[j];
        if (row.rowNumber != approval.expenseReportId) {
          continue;
        }
        // Email the employee to notify the Manager's decision about the expense report.
        sendApprovalResults(row, approval);
        // Update the state of the report to APPROVED or DENIED
        expenseSheet.getRange(row.rowNumber, COLUMN_STATE).setValue(row.state);
        break;
      }
    }
  }   
}



  //////////////////////////////
 // Email approval functions //
//////////////////////////////

// Sends an email to an employee to communicate the manager's decision on a given Expense Report.
function sendApprovalResults(row, approval) {
  var approvedOrRejected = (approval.approveExpenseReport == "Yes") ? "approved" : "rejected";
  var message = "<HTML><BODY>"
    + "<P>" + approval.emailAddress + " has " + approvedOrRejected + " your expense report."
    + "<P>Amount: £" + row.amount
    + "<P>Description: " + row.description
    + "<P>Report Id: " + row.rowNumber
    + "<P>Manager's comment: " + (approval.comments || "")
    + "</HTML></BODY>";
  MailApp.sendEmail(row.emailAddress, "Expense Report Approval Results", "", {htmlBody: message});
  if (approval.approveExpenseReport == "Yes") {
    row.state = STATE_APPROVED;
  } else {
    row.state = STATE_DENIED;
  }
}

// Sends an email to a manager to request his approval of an employee expense report.
function sendReportToManager(row) {
  var message = "<HTML><BODY>"
    + "<P>" + row.emailAddress + " has requested your approval for an expense report."
    + "<P>" + "Amount: £" + row.amount
    + "<P>" + "Type: " + row.expenseType
    + "<P>" + "Description: " + row.description
    + "<P>" + "Date: " + row.expenseDate
    + "<P>" + "Report Id: " + row.rowNumber
    + "<P>" + "Receipt: " + row.receipt
    + '<P>Please approve or reject the expense report <A HREF="' + APPROVAL_FORM_URL + '&entry_0='+row.emailAddress + '&entry_1='+ row.rowNumber +'">here</A>.'
    + "</HTML></BODY>";
  MailApp.sendEmail(row.managersEmailAddress, "Expense Report Approval Request", "", {htmlBody: message});
  row.state = STATE_MANAGER_EMAIL;
}




  /////////////////////////////////////
 // Ancillary Spreadsheet functions //
/////////////////////////////////////

// getRowsData iterates row by row in the input range and returns an array of objects.
// Each object contains all the data for a given row, indexed by its normalized column name.
// Arguments:
//   - sheet: the sheet object that contains the data to be processed
//   - range: the exact range of cells where the data is stored
//       This argument is optional and it defaults to all the cells except those in the first row
//       or all the cells below columnHeadersRowIndex (if defined).
//   - columnHeadersRowIndex: specifies the row number where the column names are stored.
//       This argument is optional and it defaults to the row immediately above range; 
// Returns an Array of objects.
function getRowsData(sheet, range, columnHeadersRowIndex) {
  var headersIndex = columnHeadersRowIndex || range ? range.getRowIndex() - 1 : 1;
  var dataRange = range || 
    sheet.getRange(headersIndex + 1, 1, sheet.getMaxRows() - headersIndex, sheet.getMaxColumns());
  var numColumns = dataRange.getEndColumn() - dataRange.getColumn() + 1;
  var headersRange = sheet.getRange(headersIndex, dataRange.getColumn(), 1, numColumns);
  var headers = headersRange.getValues()[0];
  return getObjects(dataRange.getValues(), normalizeHeaders(headers));
}

// For every row of data in data, generates an object that contains the data. Names of
// object fields are defined in keys.
// Arguments:
//   - data: JavaScript 2d array
//   - keys: Array of Strings that define the property names for the objects to create
function getObjects(data, keys) {
  var objects = [];
  for (var i = 0; i < data.length; ++i) {
    var object = {};
    var hasData = false;
    for (var j = 0; j < data[i].length; ++j) {
      var cellData = data[i][j];
      if (isCellEmpty(cellData)) {
        continue;
      }
      object[keys[j]] = cellData;
      hasData = true;
    }
    if (hasData) {
      objects.push(object);
    }
  }
  return objects;
}

// Returns an Array of normalized Strings. 
// Empty Strings are returned for all Strings that could not be successfully normalized.
// Arguments:
//   - headers: Array of Strings to normalize
function normalizeHeaders(headers) {
  var keys = [];
  for (var i = 0; i < headers.length; ++i) {
    keys.push(normalizeHeader(headers[i]));
  }
  return keys;
}

// Normalizes a string, by removing all alphanumeric characters and using mixed case
// to separate words. The output will always start with a lower case letter.
// This function is designed to produce JavaScript object property names.
// Arguments:
//   - header: string to normalize
// Examples:
//   "First Name" -> "firstName"
//   "Market Cap (millions) -> "marketCapMillions
//   "1 number at the beginning is ignored" -> "numberAtTheBeginningIsIgnored"
function normalizeHeader(header) {
  var key = "";
  var upperCase = false;
  for (var i = 0; i < header.length; ++i) {
    var letter = header[i];
    if (letter == " " && key.length > 0) {
      upperCase = true;
      continue;
    }
    if (!isAlnum(letter)) {
      continue;
    }
    if (key.length == 0 && isDigit(letter)) {
      continue; // first character must be a letter
    }
    if (upperCase) {
      upperCase = false;
      key += letter.toUpperCase();
    } else {
      key += letter.toLowerCase();
    }
  }
  return key;
}

// Returns true if the cell where cellData was read from is empty.
// Arguments:
//   - cellData: string
function isCellEmpty(cellData) {
  return typeof(cellData) == "string" && cellData == "";
}

// Returns true if the character char is alphabetical, false otherwise.
function isAlnum(char) {
  return char >= 'A' && char <= 'Z' ||
    char >= 'a' && char <= 'z' ||
    isDigit(char);
}

// Returns true if the character char is a digit, false otherwise.
function isDigit(char) {
  return char >= '0' && char <= '9';
}

