var LEGAL_CONTRACTS_SS_ID = "0AvqYxIrouBHfdDh6aDY0MHpFWVRqcVJzQjVkVGE0a1E";

var FEEDBACK_SS_ID = "0AvqYxIrouBHfdHI2bmdFblEwYng3QWdkSERjN1VwSlE";
var FEEDBACK_FORM = "https://docs.google.com/a/cloudreach.co.uk/forms/d/1B6ur2FOTRlSuySFaYyIWn08y1CXZdjqV-eDqT8RjIfg/viewform";
var FEEDBACK_PREFILL_FORM = "https://docs.google.com/forms/d/1B6ur2FOTRlSuySFaYyIWn08y1CXZdjqV-eDqT8RjIfg/viewform?entry.1660141235=100&entry.965457831=Contract1&entry.578090022=Desc&entry.225432313=01-Dec-2012&entry.73134454=01-Dec-2013";

var STATE_ADMIN_EMAIL = "james.attard@cloudreach.co.uk";

var COLUMN_ID = 1;
var COLUMN_TITLE = 2;
var COLUMN_DESCRIPTION = 3;
var COLUMN_START_DATE = 4;
var COLUMN_END_DATE = 5; 
var COLUMN_REQUIRE_INFO = 6;
var COLUMN_REQUESTED = 8;

function scanContracts(){
  
  var contractsSpreadsheet = SpreadsheetApp.openById(LEGAL_CONTRACTS_SS_ID);
  var contractsSheet = contractsSpreadsheet.getSheets()[0];

  var feedbackSpreadsheet = SpreadsheetApp.openById(FEEDBACK_SS_ID);
  var feedbackSheet = feedbackSpreadsheet.getSheets()[0];
  
  var contractData = getRowsData(contractsSheet);
  var feedbackData = getRowsData(feedbackSheet);
  
  // For every contract
  for (var i = 0; i < contractData.length; ++i) {
    var row = contractData[i];
    row.rowNumber = i + 2;
    if (row.requireInformation == "Yes") {
      // PA requires more information from contract user
      if (!row.requested) {
        // New entry
        // Email the contract owner to request his feedback
        sendContractToUser(row);
        // Update the state of the report to avoid email sending multiple emails
        // to contract owners about the same report.
        contractsSheet.getRange(row.rowNumber, COLUMN_REQUESTED+1).setValue("Yes");
      } else {
        // This contract has already been sent to the owner for feedback.
        // Check if the user has updated the contract in the Feedback Spreadsheet.
        for (var j = 0; j < feedbackData.length; ++j) {
          var feedback = feedbackData[j];
          if (row.contractId != feedback.contractId) {
            continue;
          }
          // Email the PA to notify that user has updated contract.
          sendUpdatedNotification(row);
          // Update information on main spreadsheet (Legal Contracts Input Form)
          row.contractId = feedback.contractId;
          row.contractTitle = feedback.contractTitle;
          row.description = feedback.description;
          row.startDate = feedback.startDate;
          row.endDate = feedback.endDate;
          contractsSheet.getRange(row.rowNumber, COLUMN_ID+1).setValue(row.contractId);
          contractsSheet.getRange(row.rowNumber, COLUMN_TITLE+1).setValue(row.contractTitle);
          contractsSheet.getRange(row.rowNumber, COLUMN_DESCRIPTION+1).setValue(row.description);
          contractsSheet.getRange(row.rowNumber, COLUMN_START_DATE+1).setValue(row.startDate);
          contractsSheet.getRange(row.rowNumber, COLUMN_END_DATE+1).setValue(row.endDate);
          contractsSheet.getRange(row.rowNumber, COLUMN_REQUIRE_INFO+1).setValue("No");
          break;
        }
      }
  }   
 }  
}



// Sends an email to contract user to fill missing information from contract.
function sendContractToUser(row) {
  var message = "<HTML><BODY>"
    + "<P>" + "You are requested to provide feedback to update a Legal Contract."
    + '<P>Please complete missing information by clicking <A HREF="' + FEEDBACK_FORM +  '?entry.1660141235=' + row.contractId + '&entry.965457831=' + row.contractTitle + '&entry.578090022=' + row.description + '&entry.225432313=' + row.startDate + '&entry.73134454=' + row.endDate + '">here</A>.'    
    + "</HTML></BODY>";
  MailApp.sendEmail(row.contractUserEmail, "Legal Contract Information Request", "", {htmlBody: message});
}

// Sends an email to PA to inform that contract has been updated
function sendUpdatedNotification(row) {
  var message = "<HTML><BODY>"
    + "<P>" + row.contractUserEmail + " has updated contract " + row.contractId;
  MailApp.sendEmail(STATE_ADMIN_EMAIL, "Legal Contract has been updated", "", {htmlBody: message});
}


function getRowsData(sheet, range, columnHeadersRowIndex) {
  var headersIndex = columnHeadersRowIndex || range ? range.getRowIndex() - 1 : 1;
  var dataRange = range || 
    sheet.getRange(headersIndex + 1, 1, sheet.getMaxRows() - headersIndex, sheet.getMaxColumns());
  var numColumns = dataRange.getEndColumn() - dataRange.getColumn() + 1;
  var headersRange = sheet.getRange(headersIndex, dataRange.getColumn(), 1, numColumns);
  var headers = headersRange.getValues()[0];
  return getObjects(dataRange.getValues(), normalizeHeaders(headers));
}

// For every row of data in data, generates an object that contains the data. Names of
// object fields are defined in keys.
// Arguments:
//   - data: JavaScript 2d array
//   - keys: Array of Strings that define the property names for the objects to create
function getObjects(data, keys) {
  var objects = [];
  for (var i = 0; i < data.length; ++i) {
    var object = {};
    var hasData = false;
    for (var j = 0; j < data[i].length; ++j) {
      var cellData = data[i][j];
      if (isCellEmpty(cellData)) {
        continue;
      }
      object[keys[j]] = cellData;
      hasData = true;
    }
    if (hasData) {
      objects.push(object);
    }
  }
  return objects;
}

// Returns an Array of normalized Strings. 
// Empty Strings are returned for all Strings that could not be successfully normalized.
// Arguments:
//   - headers: Array of Strings to normalize
function normalizeHeaders(headers) {
  var keys = [];
  for (var i = 0; i < headers.length; ++i) {
    keys.push(normalizeHeader(headers[i]));
  }
  return keys;
}

// Normalizes a string, by removing all alphanumeric characters and using mixed case
// to separate words. The output will always start with a lower case letter.
// This function is designed to produce JavaScript object property names.
// Arguments:
//   - header: string to normalize
// Examples:
//   "First Name" -> "firstName"
//   "Market Cap (millions) -> "marketCapMillions
//   "1 number at the beginning is ignored" -> "numberAtTheBeginningIsIgnored"
function normalizeHeader(header) {
  var key = "";
  var upperCase = false;
  for (var i = 0; i < header.length; ++i) {
    var letter = header[i];
    if (letter == " " && key.length > 0) {
      upperCase = true;
      continue;
    }
    if (!isAlnum(letter)) {
      continue;
    }
    if (key.length == 0 && isDigit(letter)) {
      continue; // first character must be a letter
    }
    if (upperCase) {
      upperCase = false;
      key += letter.toUpperCase();
    } else {
      key += letter.toLowerCase();
    }
  }
  return key;
}

// Returns true if the cell where cellData was read from is empty.
// Arguments:
//   - cellData: string
function isCellEmpty(cellData) {
  return typeof(cellData) == "string" && cellData == "";
}

// Returns true if the character char is alphabetical, false otherwise.
function isAlnum(char) {
  return char >= 'A' && char <= 'Z' ||
    char >= 'a' && char <= 'z' ||
    isDigit(char);
}

// Returns true if the character char is a digit, false otherwise.
function isDigit(char) {
  return char >= '0' && char <= '9';
}






function deleteOutdatedRow(contractId) {

  var contractsSpreadsheet = SpreadsheetApp.openById(LEGAL_CONTRACTS_SS_ID);
  var contractsSheet = contractsSpreadsheet.getSheets()[0];
  var values = contractsSheet.getDataRange().getValues();

  for( var row = 0; row < values.length ; row ++ ){
    if (values[row][1] == contractId){
      contractsSheet.deleteRow(row+1);
      break;
    }
  }
};

